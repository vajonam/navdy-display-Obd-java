package com.navdy.obd.app;

import android.content.Context;
import android.widget.Toast;

public class Utility {
    public static void toast(Context context, String message) {
        Toast.makeText(context, message, 0).show();
    }

    public static void toast(Context context, int res) {
        Toast.makeText(context, res, 0).show();
    }

    public static void toastNotConnectedMessage(Context context) {
        Toast.makeText(context, R.string.not_connected, 0).show();
    }
}
