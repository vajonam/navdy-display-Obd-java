package com.navdy.obd.converters;

public class TemperatureConversion implements AbstractConversion {
    public double convert(byte[] rawData) {
        if (rawData != null) {
            return (double) ((rawData[2] & 255) - 40);
        }
        return 0.0d;
    }
}
