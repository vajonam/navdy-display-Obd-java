package com.navdy.obd;

import java.util.List;

public class PidLookupTable {
    private Pid[] mSnapshot;

    public PidLookupTable(int capacity) {
        this.mSnapshot = new Pid[capacity];
    }

    public void build(List<Pid> pids) {
        this.mSnapshot = new Pid[this.mSnapshot.length];
        for (Pid pid : pids) {
            if (pid.getId() < this.mSnapshot.length) {
                this.mSnapshot[pid.getId()] = pid;
            }
        }
    }

    public Pid getPid(int id) {
        if (validId(id)) {
            return this.mSnapshot[id];
        }
        return null;
    }

    private boolean validId(int id) {
        return id >= 0 && id < this.mSnapshot.length;
    }

    public double getPidValue(int id) {
        Pid pid = getPid(id);
        return pid != null ? pid.getValue() : -1.0d;
    }

    public boolean updatePid(int id, double value) {
        Pid pid = getPid(id);
        boolean changed = false;
        if (pid != null) {
            changed = pid.getValue() != value;
            pid.setValue(value);
        }
        return changed;
    }
}
