package com.navdy.obd;

import android.os.Build;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;

public class Utility {
    public static int updateCcittCrc(int crc, byte data) {
        int x = ((crc >> 8) ^ data) & 255;
        x ^= x >> 4;
        return (((crc << 8) ^ (x << 12)) ^ (x << 5)) ^ x;
    }

    public static int calculateCcittCrc(byte[] data) {
        int crc = 0;
        for (byte input : data) {
            crc = updateCcittCrc(crc, input);
        }
        return crc;
    }

    public static double calculateInstantaneousFuelConsumption(double maf, double vss) {
        if (vss > 0.0d) {
            return (34.0278d * maf) / vss;
        }
        return 0.0d;
    }

    public static String convertInputStreamToString(InputStream inputStream) {
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            byte[] buffer = new byte[16384];
            while (true) {
                int n = inputStream.read(buffer);
                if (n == -1) {
                    return byteArrayOutputStream.toString();
                }
                byteArrayOutputStream.write(buffer, 0, n);
            }
        } catch (Throwable th) {
            return null;
        }
    }

    public static int bytesToInt(byte[] byteArray, int offset, int length, boolean bigEndian) {
        if (byteArray == null || length <= 0 || length > 4 || byteArray.length < offset + length) {
            throw new NumberFormatException("Incorrect data");
        }
        int data = 0;
        for (int i = 0; i < length; i++) {
            data += byteArray[offset + (bigEndian ? i : (length - i) - 1)] & 255;
            if (i < length - 1) {
                data <<= 8;
            }
        }
        return data;
    }

    public static boolean isUserBuild() {
        return "user".equals(Build.TYPE);
    }
}
