package com.navdy.obd;

import com.navdy.obd.command.IObdDataObserver;
import com.navdy.obd.command.ObdCommand;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ObdDataObserver implements IObdDataObserver {
    static final Logger CAN_RAW_LOG = LoggerFactory.getLogger(ObdService.CAN_BUS_LOGGER);
    static final Logger OBD_RAW_LOG = LoggerFactory.getLogger("com.navdy.obd.ObdRaw");

    public void onCommand(String command) {
        if ( ObdCommand.doLogRaw) OBD_RAW_LOG.debug("C,{}", command);
    }

    public void onResponse(String response) {
        if ( ObdCommand.doLogRaw) OBD_RAW_LOG.debug("R,{}", response);
    }

    public void onError(String error) {
        if ( ObdCommand.doLogRaw) OBD_RAW_LOG.error("E,{}", error);
    }

    public void onRawCanBusMessage(String message) {
        if ( ObdCommand.doLogRaw) CAN_RAW_LOG.debug(message);
    }
}
