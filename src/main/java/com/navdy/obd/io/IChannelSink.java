package com.navdy.obd.io;

public interface IChannelSink {
    void onMessage(String str);

    void onStateChange(int i);
}
