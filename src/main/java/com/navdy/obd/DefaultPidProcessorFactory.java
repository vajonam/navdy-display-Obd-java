package com.navdy.obd;

import java.util.ArrayList;
import java.util.List;

public class DefaultPidProcessorFactory implements PidProcessorFactory {
    private static List<Integer> PIDS_WITH_PRE_PROCESSORS = new ArrayList();

    static {
        PIDS_WITH_PRE_PROCESSORS.add(Integer.valueOf(256));
        PIDS_WITH_PRE_PROCESSORS.add(Integer.valueOf(47));
        PIDS_WITH_PRE_PROCESSORS.add(Integer.valueOf(13));
    }

    public PidProcessor buildPidProcessorForPid(int pid) {
        switch (pid) {
            case 13:
                return new SpeedPidProcessor();
            case 47:
                return new FuelLevelPidProcessor();
            case 256:
                return new InstantFuelConsumptionPidProcessor();
            default:
                return null;
        }
    }

    public List<Integer> getPidsHavingProcessors() {
        return PIDS_WITH_PRE_PROCESSORS;
    }
}
