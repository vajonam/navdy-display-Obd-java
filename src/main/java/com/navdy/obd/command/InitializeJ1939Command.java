package com.navdy.obd.command;

public class InitializeJ1939Command extends BaseSTNInitializeCommand {
    private static ObdCommand setProtocolCommand = new ObdCommand("SetProtocol", "atsp" + Integer.toHexString(10));

    public InitializeJ1939Command() {
        super(ObdCommand.RESET_COMMAND, ObdCommand.ECHO_OFF_COMMAND, ObdCommand.SPACES_OFF_COMMAND, ObdCommand.TURN_OFF_VOLTAGE_LEVEL_WAKEUP);
        add(setProtocolCommand);
    }
}
