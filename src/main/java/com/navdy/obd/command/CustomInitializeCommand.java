package com.navdy.obd.command;

import com.navdy.obd.Protocol;
import com.navdy.os.SystemProperties;

public class CustomInitializeCommand extends BaseSTNInitializeCommand {
    private ObdCommand readProtocolCommand = ObdCommand.READ_PROTOCOL_COMMAND;
    private VinCommand vinCommand = new VinCommand();

    public CustomInitializeCommand(String[] commands) {
        super();
        for (String com : commands) {
            add(new ObdCommand(com));
        }
        add(ObdCommand.DETECT_PROTOCOL_COMMAND);
        add(this.readProtocolCommand);
    }

    public String getProtocol() {
        return this.readProtocolCommand.getResponse();
    }
}
