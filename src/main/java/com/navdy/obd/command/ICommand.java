package com.navdy.obd.command;

import com.navdy.obd.Protocol;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public interface ICommand {
    void execute(InputStream inputStream, OutputStream outputStream, Protocol protocol, IObdDataObserver iObdDataObserver) throws IOException;

    double getDoubleValue();

    int getIntValue();

    String getName();

    String getResponse();
}
