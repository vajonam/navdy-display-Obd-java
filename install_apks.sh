#!/system/bin/sh

if [[ -f /maps/Obd.apk ]]; then
	am force-stop com.navdy.obd.app
	sleep 2
	su -c 'rm /system/priv-app/Obd/Obd.apk'
	su -c 'ln -s /maps/Obd.apk /system/priv-app/Obd/'

	su -c 'rm -rf "/data/dalvik-cache/arm/system@priv-app@Obd@Obd.apk@classes.dex"'
	su -c 'rm -rf /data/dalvik-cache/profiles/com.navdy.obd.app'

	am start -S -n com.navdy.hud.app/.ui.activity.MainActivity
fi
